import os , sys , subprocess

key = "/home/hadoop/pmoy-insight.pem"
slaves = subprocess.check_output ( 'bash ~/list_slaves.sh' , shell = True ).split()

for patch in sys.argv[1:]:
    for s in slaves:
        os.system ( "scp -o StrictHostKeyChecking=no -i {0} {1} hadoop@{2}:~".format ( key , patch , s ) )
        os.system ( "ssh -o StrictHostKeyChecking=no -i {0} hadoop@{1} 'bash ~/{2}'".format ( key , s , patch ) )
    
