#!/usr/bin/env python
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.log import enable_pretty_logging
from app import app
import logging
logging.basicConfig(filename="/home/ubuntu/themidgame-tube/tornado.log",filemode='a',level=logging.CRITICAL,format='%(asctime)s %(message)s')
if False:
    app.run(host='0.0.0.0',port=80, debug = True)
else:
    enable_pretty_logging()
    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen("80")
    IOLoop.instance().start()

