from flask import render_template
from flask import jsonify
from flask import request
import subprocess
from app import app
import requests
import shelve
import os , sys
import random

from flask_limiter import Limiter
limiter = Limiter(app)

@app.route('/')
@app.route("/home")
@limiter.limit("10/second")
def home():
    return render_template("index.html")

@app.route("/contact")
@limiter.limit("1/second")
def show_contact():
    return render_template("contacts.html")
