import requests
import re
import string
import json
import os , sys
import getopt
import traceback

class DataExtractor:
    key = "AIzaSyAIIFhI6xx-vkljaq-0FxSCVa709hZUBPY"
    def __init__(self,dirname,vid_link=None):
        self.dirname = dirname
        self.data = { }
        if vid_link is not None:
            self.vid_id = vid_link.split("=")[1]
            self.data [ self.vid_id ] = { }
    def set_vid_link(self,vid_link):
            self.vid_id = vid_link.split("=")[1]
            self.data [ self.vid_id ] = { }
    def get_transcript(self):
        r = requests.get('http://keepsubs.com/?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D' + self.vid_id )
        pat = r'"(http://keepsubs.com/subs/youtube.com.php?.+)"'
        match = re.search ( pat , r.text )
        try:
            url = match.groups(0)[0].split ('"')[0]
            r = requests.get(url).text
            r = filter ( lambda x: x in string.printable , r )
            self.data [ self.vid_id ]["transcript"] = r
        except:
            print "No transcript :("
            self.data [ self .vid_id]["transcript"] = ""
    def get_channel (self,cid):
        url_api = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id={ID}&fields=items/statistics/commentCount,items/statistics/subscriberCount,items/statistics/videoCount,items/statistics/viewCount&key={YOUR_API_KEY}"
        url_call = url_api.format ( ID = cid , YOUR_API_KEY = self.key )
        r = requests.get ( url_call ).text
        self.data[self.vid_id]["channel"] = r
    def get_description(self):
        url_api = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id={VIDEO_ID}&fields=items/snippet/channelId,items/snippet/publishedAt,items/snippet/title,items/snippet/description,items/snippet/channelTitle&key={YOUR_API_KEY}"
        url_call = url_api.format ( VIDEO_ID = self.vid_id , YOUR_API_KEY = self.key )
        r = requests.get ( url_call ).text
        r = filter ( lambda x: x in string.printable , r )
        self.data [ self.vid_id ]["description"] = r
        try:
            self.get_channel(eval(r)["items"][0]["snippet"]["channelId"])
        except:
            print traceback.format_exc()
            print "No channel data? :("
            self.data[self.vid_id]["channel"] = ""
    def get_stats(self):
        url_api = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id={VIDEO_ID}&fields=items/statistics/commentCount,items/statistics/dislikeCount,items/statistics/likeCount,items/statistics/viewCount&key={YOUR_API_KEY}"
        url_call = url_api.format ( VIDEO_ID = self.vid_id , YOUR_API_KEY = self.key )
        r = requests.get ( url_call ).text
        r = filter ( lambda x: x in string.printable , r )
        self.data [ self.vid_id ]["stats"] = r
    def get_comments(self):
        api_call = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet%2Creplies&videoId={videoId}&maxResults=100&key={YOUR_API_KEY}"
        ready_call = api_call.format ( videoId =  self.vid_id , YOUR_API_KEY = self.key )
        r = requests.get(ready_call).text
        r = filter ( lambda x: x in string.printable , r )
        self.data [ self.vid_id ]["comments"] = r
    def get_data(self,vid_link , container ):
        self.set_vid_link ( vid_link )
        self.get_transcript()
        self.get_description()
        self.get_comments()
        self.get_stats()
        self.data [ self.vid_id ]["container"] = container
    def output_data(self,output_name):
        with open(os.path.join ( self.dirname , output_name ) ,"wb") as f:
            json.dump ( self.data , f )

if __name__ == "__main__":
    #GET https://www.googleapis.com/youtube/v3/search?part=snippet&relatedToVideoId=5rOiW_xY-kc&type=video&key={YOUR_API_KEY}

    dirname = "/home/hadoop/themidgame-tube/scraper"
    opts , args = getopt.getopt ( sys.argv[1:] , '' , [ "dirname=","sample"] )
    sample = False
    for opt , val in opts:
        if "sample" in opt:
            sample = True
        if  "dirname" in opt:
            dirname = val

    dataObject = DataExtractor(dirname)

    brand = None
    with open ( args[0] ) as f:
        brand = "-".join(os.path.split(f.name)[1].split("-")[3:])
        dirn,fname = os.path.split ( f.name )
        outfile = "DATA_" + fname
        data = f.read()
        try:
            links = eval ( data )
        except SyntaxError:
            if data[-1] == ",":
                data = data [:-1] + "]"
            try:
                links = eval ( data )
            except SyntaxError:
                print
                print "The data is not good ;("
                #print data
                print
                links = [ ]

    if sample:
        links = links[:5]
        outfile = "SAMPLE_" + outfile

    i = 0
    maxi = 1000
    for vid_link in links:
        if i > maxi: break
        if "user:" in vid_link: continue
        link = vid_link [ "href" ]
        print "Getting data for brand {0}: {1}".format ( brand , link.strip() )
        try:
            dataObject.get_data( link.strip() , outfile )
        except:
            print traceback.format_exc()
            print "Could not get " , link
        i += 1
    dataObject.output_data(outfile)
