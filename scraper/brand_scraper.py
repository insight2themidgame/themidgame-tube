import requests
import string
urls = [ "http://www.namedevelopment.com/brand-names.html#0", "http://www.namedevelopment.com/brand-names.html#27" , "http://www.namedevelopment.com/brand-names.html#55" , "http://www.namedevelopment.com/brand-names.html#83" , "http://www.namedevelopment.com/brand-names.html#139" ]

data = [ ]

def getBrands ( url ):
    r = requests.get ( url ).text.split ("\n")
    for x in r:
        if "<li>" in x:
            d = x.strip().replace("<li>","").replace("</li>","").strip()
            d = filter ( lambda x : x in string.printable , d )
            if "<" not in d and ">" not in d:
                data.append (d)

for url in urls:
	getBrands ( url )

with open("world_brands.txt","wb") as f:
	f.write ("\n".join(data))
