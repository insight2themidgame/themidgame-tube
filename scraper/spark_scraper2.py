import getopt
import os , sys
import random
import glob
import subprocess
import multiprocessing
from multiprocessing import Pool
import getopt

with open ( "/home/hadoop/themidgame-tube/script_config.json" ) as f:
    sdata = eval ( f.read() )
sb = sdata["bucket"]

opts,args=getopt.getopt ( sys.argv[1:] , "" , ["test","single"] )
test = False
single = False
for opt,val in opts:
    if "test" in opt:
        test = True
    if "single" in opt:
        single = True
dataf = ""
if test: dataf = "--sample "

with open ( "world_brands.txt" ) as f:
    brands = f.readlines()
    brands = map ( lambda xs : xs.strip() , brands )
    if test:
        brands = brands[:5]

#dtarget = os.getcwd()
dtarget = "/mnt/themidgame-tube-scraper"
if not os.path.isdir ( dtarget ): os.mkdir ( dtarget )
j = os.path.join

def get_links ( brand ):
    print "Scraping for {0}".format ( brand )
    brand = brand.strip().split("(")[0]
    brand = "+".join ( brand.split () )
    brand = brand.replace ( "'" , "%27" )
    brand = brand.replace ( "&" , "%26" )
    brand = brand.replace ( ";" , "%3B" )
    brand = brand.replace ( "," , "%2C" )
    brand = brand.replace ( ":" , "%3A" )
    try:
        os.system ( "node /home/hadoop/themidgame-tube/scraper/index.js {0} {1}".format ( dtarget , "https://www.youtube.com/results?search_query="+brand ) )
    except:
        pass
    print "Finished scraping.."
    sys.stdout.flush()

def get_data ( r ):
    print "Scraping YouTube API for {0}".format ( r )
    try:
        os.system ( "python /home/hadoop/themidgame-tube/scraper/data_extractor.py {2}--dirname={0} {1}".format ( dtarget , r , dataf ) )
    except:
        pass
    print "Finished scraping.."
    sys.stdout.flush()

def send_link ( r ):
    try:
        os.system ( "aws s3 cp {0} s3://{2}/{1}/".format ( r , sdata["id_search_results"],sb) )
        os.remove ( r )
    except:
        pass

def send_data ( d ):
    try:
        os.system ( "aws s3 cp {0} s3://{2}/{1}/".format ( d , sdata["raw_data_dump"] , sb) )
        os.remove ( d )
    except:
        pass

if not single:
    #nproc = multiprocessing.cpu_count()
    nproc = int ( subprocess.check_output ( "python ~/themidgame-tube/tools/get_thread_num.py" , shell = True ) )
    p = Pool ( nproc )
    n = 0
    while True:
        n += 1
        print "Starting scrapper process {0}".format ( n )
        sys.stdout.flush()
        results = glob.glob ( j( dtarget , "results-*" ) )
        if not results:
            p.map ( get_links , brands )
            results = glob.glob ( j( dtarget , "results-*" ) )
        chunks=[results[x:x+nproc] for x in xrange(0, len(results), nproc)]
        print "Finished search.."
        sys.stdout.flush()
        for c in chunks:
            print "Processing {0} brands".format ( len ( c ) )
            sys.stdout.flush()
            data = glob.glob ( j ( dtarget , "DATA_*" ) )
            data.extend( glob.glob ( j ( dtarget , "SAMPLE_DATA_*" ) ) )
            print "Scraping {0} sets..".format ( len ( data ) )
            sys.stdout.flush()
            if not data:
                p.map ( get_data , c )
                data = glob.glob ( j ( dtarget , "DATA_*" ) )
                data.extend( glob.glob ( j ( dtarget , "SAMPLE_DATA_*" ) ) )
            print "Sending data.."
            sys.stdout.flush()
            p.map ( send_data , data )
        print "Sending searches.."
        sys.stdout.flush()
        p.map ( send_link , results )
        os.sytem ( "rm core*" )
else:
    while True:
        for b in brands:
            get_brand ( b )
