var Xray = require('x-ray');
var x = Xray();
var randomstring = require("randomstring");

var dirname = "/home/hadoop/themidgame-tube/scraper"
var args = process.argv
if (args.length==4){
    dirname = args[2]
    url = args[3]
} else {
    url = args[2]
}

function scrape(url, filename) {
  return x(url, 'a.yt-uix-tile-link[href*=watch]', [{
    title: '@title',
    href: '@href'
  }])
    .paginate('a[data-link-type=next]@href')
    .write(filename);
}

function run(url) {
  if (!url) {
    throw 'please especify a url "node index.js http..."';
  }
  var time = (new Date()).getTime().toString(),
    filename = dirname + "/" + 'results-'+ randomstring.generate() + '-' +time + '-' + url.split ( '=' )[1].replace ( /\+/g , '_' ) + '.json',
    stream = scrape(url, filename);
/*
  stream.on('finish', function () {
    console.log('done');
  });
  stream.on('error', function (error) {
    console.error('error scraping')
    console.error(error);
  });
*/
};

run(url);
