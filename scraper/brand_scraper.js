var Xray = require('x-ray'),
  x = Xray();

function getLetters() {
  var letters = [];
  addCharsFromTo(48, 57, letters);
  addCharsFromTo(97, 122, letters);

  return letters;
}

function addCharsFromTo(starting, ending, destination) {
  for (var i = starting; i <= ending; i++) {
    destination.push(String.fromCharCode(i));
  }
}

function scrapeBrandsStartingWithCharacter(character) {
  var baseUrl = 'http://www.brandsoftheworld.com/logos/letter/',
    url = baseUrl + character,
    filename = 'results.' + character + '.json',
    selector = ['a[href*="/logo/' + character + '"]'],
    nextPageSelector = '.pager-next a@href',
    structure = [{
      // title: '@text',
      href: '@href'
    }],
    options = {
      url: url,
      selector: selector,
      // structure: structure,
      nextPageSelector: nextPageSelector,
      filename: filename
    };

    scrapeUrl(options);
}

function scrapeUrl(options) {
  x(options.url, options.selector, options.structure)
  .paginate(options.nextPageSelector)
  .write(options.filename);
}

getLetters().forEach(scrapeBrandsStartingWithCharacter);
