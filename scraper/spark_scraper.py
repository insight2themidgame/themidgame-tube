import getopt
import os , sys
import random
import glob
import subprocess
try:
    from pyspark import SparkContext
    spark = True
except ImportError:
    spark = False
import getopt

with open ( "/home/hadoop/themidgame-tube/script_config.json" ) as f:
    sdata = eval ( f.read() )
sb = sdata["bucket"]

opts,args=getopt.getopt ( sys.argv[1:] , "" , ["test","single"] )
test = False
single = False
for opt,val in opts:
    if "test" in opt:
        test = True
    if "single" in opt:
        single = True
dataf = ""
if test: dataf = "--sample "


with open ( "world_brands.txt" ) as f:
    brands = f.read().split()
    if test:
        brands = [brands[0]]

print test , single , dataf, brands

j = os.path.join
def get_brand ( brand ) :
    dtarget = os.getcwd()
    print "Scraping for {0}".format ( brand )
    os.system ( "node /home/hadoop/themidgame-tube/scraper/index.js {0} {1}".format ( dtarget , "https://www.youtube.com/results?search_query="+brand.strip() ) )
    results = glob.glob ( j ( dtarget , "results-*" ) )
    print "results",results
    for r in results:
        os.system ( "aws s3 cp {0} s3://{2}/{1}/".format ( r , sdata["id_search_results"],sb) )
    for r in results:
        os.system ( "python /home/hadoop/themidgame-tube/scraper/data_extractor.py {2}--dirname={0} {1}".format ( dtarget , r , dataf ) )
        os.remove ( r )
    data = glob.glob ( j ( dtarget , "DATA_*" ) )
    data.extend( glob.glob ( j ( dtarget , "SAMPLE_DATA_*" ) ) )
    print "data" , data
    for d in data:
        os.system ( "aws s3 cp {0} s3://{2}/{1}/".format ( d , sdata["raw_data_dump"] , sb) )
        os.remove ( d )

if spark:
    nproc = int ( subprocess.check_output ( "python ~/themidgame-tube/tools/get_thread_num.py" , shell = True ) )
    if single: nproc = 1
    sc = SparkContext ( appName = "themidgame-tube_scraper" )
    while True:
        sc_brands = sc.parallelize ( brands , nproc )
        sc_brands.foreach ( get_brand )
else:
    while True:
        for b in brands:
            get_brand ( b )
