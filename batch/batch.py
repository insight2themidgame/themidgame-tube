import logging
from hbase_batch import *
from pyspark import SparkContext
import os , sys , subprocess , shelve

logging.basicConfig(filename="themidgame-tube.log",filemode='a',level=logging.INFO,format='%(asctime)s %(message)s')
logging.info ("START BATCH")
logging.info ( "ARGS: {0}".format ( str(sys.argv) ) )

host = subprocess.check_output ( "bash /home/hadoop/get_public.sh" , shell = True )

with open ( "/home/hadoop/themidgame-tube/script_config.json" ) as f:
    sdata = eval ( f.read() )
sb = sdata["bucket"]
dd = sdata["data"]

db0 = shelve.open ( "/home/hadoop/brands.db" , writeback = True )
db0.clear()
db0.sync()

db1 = shelve.open ( "/home/hadoop/view.db" , writeback = True )
db1 [ "view" ] = -1
db1 [ "host" ] = host
db1.sync()
view=i=0

while True:

    logging.info ( "LOAD DATA" )
    os.system ( "python /home/hadoop/themidgame-tube/batch/getData.py" )

    sc = SparkContext ( appName = "themidgame-tube_batcher" )
    data = sc.textFile ( "hdfs:///{0}".format ( dd ) , use_unicode=False ).cache()

    logging.info ( "NUMBER OF RECORDS: {0}".format (data.count()) )

    logging.info ( "LOAD DATA END" )
    logging.info ( "MAKE VIEW" )

    make_tables ( host , view )
    make_web_view2 ( data , host , view , sc , "VC", False )
    make_web_view2 ( data , host , view , sc , "SC", False )

    logging.info ( "MAKE VIEW END" )

    logging.info ( "UPDATE APP" )

    #brands0 = read_hbase ( host , hbase_tables[view+1], hbase_families[1] + ":DATA" , sc ).map( lambda xs: xs[0].split("=")[0])
    brands0 = read_hbase ( host , hbase_tables[view+1], hbase_families[1] + ":DATA" , sc ).map( lambda xs: xs[0].split("=")[0]).distinct()
    brands_list = brands0.collect()
    for b in brands_list:
        bkey = "".join(str(b).lower().split("-")[1:])
        db0 [ bkey ] = str(b)
    logging.info ( "NUMBER OF BRANDS: {0}".format (len(brands_list)) )

    db1 [ "view" ] = view
    db1.sync()
    db0.sync()
    i += 1
    view = i % 2

    logging.info ( "UPDATE APP END" )
    sc.stop()

    break

logging.info ( "START BATCH END" )

db0.close()
db1.close()
