import subprocess
import string
import happybase
import logging
import os

hbase_tables = [ "base_view0" , "web_view0" , "web_view1" ]
hbase_families = [ "base_family" , "d" , "d" ]

hbase_tables_script = [ "createBaseView.hb"  , "createWebView0.hb" , "createWebView1.hb" ]


#create tables if they dont already exist
def make_tables ( host , view ):
    conn =  happybase.Connection ( host )
    os.system ( "python makeRegions.py" )
    def aux(i):
        if hbase_tables[i] in conn.tables():
            print "Deleting table {0}..".format (hbase_tables[i])
            conn.delete_table ( hbase_tables[i] , True )
        #conn.create_table ( hbase_tables[i] , { hbase_families[i] : dict() } )
        logging.info ( "Creaing table with {0}".format ( hbase_tables_script[i] ) )
        os.system ( "hbase shell {0}".format ( hbase_tables_script [i] ) ) #creat table with pre-splits
    aux(0)
    aux(view+1)
    conn.close()


def clean_rdd ( data ):
    def make_eval (d):
        try:
            return eval(d)
        except:
            return None
    _0data = data.map(make_eval)
    _1data = _0data.filter ( lambda xs : xs != None )
    check = ['container',
     'description',
     'vid_id',
     'comments',
     'key',
     'timestamp',
     'stats',
     'transcript',
     'channel']
    def check_func(d):
        for c in check:
            if c not in d:
                return False
        return True
    data = _1data.filter ( check_func )
    data2 = data.filter ( lambda xs : len ( xs["transcript"] ) > 0 )
    return data2

def trans_link ( brand ):
    brand = brand.replace ( "_" , " " )
    brand = brand.replace ( "%27" , "'" )
    brand = brand.replace ( "%26" , "&" )
    brand = brand.replace ( "%3B" , ";" )
    brand = brand.replace ( "%2C" , "," )
    brand = brand.replace ( "%3A" , ":" )
    return brand


def get_view2(d,label):
    #"kafka_container","brand"
    aux = trans_link ( d["container"] )
    brand = ".".join( "-".join(aux.split("-")[3:]).split(".")[:-1]) 

    #salting
    brand = brand.lower()[:2] + "-" + brand

    #"channel_id","channel_title","description","published_date","title"
    aux = eval(d["description"])["items"][0]["snippet"]
    channel_id = aux["channelId"]
    channel_title = aux["channelTitle"]
    title = aux["title"]
    #"video_id"
    video_id = d["vid_id"]
    #"comment_count","dislike_count","like_count","view_count"
    aux = eval(d['stats'])["items"][0]["statistics"]
    view_count = aux["viewCount"]
    #"transcript"
    transcript = d["transcript"]
    #"channel_comment_count","channel_subscriber_count","channel_video_count","channel_view_count"
    aux = eval(d["channel"])["items"][0]["statistics"]
    channel_subscriber_count = aux["subscriberCount"] 

    if label == "VC":
        metric = view_count
    else:
        metric = channel_subscriber_count 

    #missing hbase_families[1]
    row_key1 = brand + "=" + label + "=" + metric.zfill(15) + "=" + video_id
    dlabel = "-+=!=+-"
    data_col =  title + dlabel + channel_id + dlabel + channel_title 
    transcript = get_words ( transcript )
    return ( (row_key1,hbase_families[1]) , (data_col , transcript) )


def try_get_view2(d,label):
    try:
        return get_view2(d,label)
    except:
        return []


def batch_hbase ( host , table , rdd ):
    """
    * must have created table and family in HBase first: hbase(main):001:0> create 'test', 'f1'
    table -> table[string]
    rdd[1] -> (row,family,col,value)
    """

    conf = {"hbase.zookeeper.quorum": host,
            "hbase.mapred.outputtable": table,
            "mapreduce.outputformat.class": "org.apache.hadoop.hbase.mapreduce.TableOutputFormat",
            "mapreduce.job.output.key.class": "org.apache.hadoop.hbase.io.ImmutableBytesWritable",
            "mapreduce.job.output.value.class": "org.apache.hadoop.io.Writable"}
    keyConv = "org.apache.spark.examples.pythonconverters.StringToImmutableBytesWritableConverter"
    valueConv = "org.apache.spark.examples.pythonconverters.StringListToPutConverter"

    #print rdd.take(3)
    #return

    rdd.map(lambda x: (x[0], x)).saveAsNewAPIHadoopDataset(
        conf=conf,
        keyConverter=keyConv,
        valueConverter=valueConv)


def read_hbase ( host , table, columns , sc ):

    # Other options for configuring scan behavior are available. More information available at
    # https://github.com/apache/hbase/blob/master/hbase-server/src/main/java/org/apache/hadoop/hbase/mapreduce/TableInputFormat.java
    conf = {
            "hbase.zookeeper.quorum": host, 
            "hbase.mapreduce.inputtable": table,
            "hbase.mapreduce.scan.columns" : columns
           }
    keyConv = "org.apache.spark.examples.pythonconverters.ImmutableBytesWritableToStringConverter"
    valueConv = "org.apache.spark.examples.pythonconverters.HBaseResultToStringConverter"

    hbase_rdd = sc.newAPIHadoopRDD(
        "org.apache.hadoop.hbase.mapreduce.TableInputFormat",
        "org.apache.hadoop.hbase.io.ImmutableBytesWritable",
        "org.apache.hadoop.hbase.client.Result",
        keyConverter=keyConv,
        valueConverter=valueConv,
        conf=conf)
    return hbase_rdd

def make_read_hbase ( host , table, family , columns , sc ):
    out = { }
    for c in columns:
        out [ c ] = read_hbase ( host , table, "{0}:{1}".format ( family , c ) , sc )
    return out


def get_words(test):
    test = str(test)
    cleanTrans = lambda xs : xs.translate(string.maketrans("",""),string.punctuation+string.digits).replace("xD"," ").replace("xA"," ").split()
    clean_list = map ( string.lower , cleanTrans ( test ) )
    count = { }
    for c in clean_list:
        count [ c ] = count.get(c,0)+1
    clean_list_out = [ ]
    for key,val in count.items():
        clean_list_out.append ( key + ":" + str(val) )
    return ["DATA"] + clean_list_out

def link_data (xs):
    out = []
    data = xs[0]
    tags = xs[1]
    for t in tags:
        if t == "DATA":
            out.append ( ("DATA" , data) )
        else:
            t = t.split(":")
            out.append ( (t[0], t[1]) )
    return tuple(out)


def make_web_view2(data,host,view,sc,label,ret):
    """
    label = "VC" ; metric = "view_count"
    label = "SC" ; metric = "channel_subscriber_count"
    """
    data2 = clean_rdd ( data )
    data3 = data2.map(lambda xs : try_get_view2(xs,label)).filter(lambda xs: len(xs)>0)
    data_col3 = data3.map ( lambda xs: (xs[0], link_data(xs[1])) )
    #data_col3 = data3.map ( lambda xs: (xs[0], link_data(xs[1])) ).reduceByKey(lambda x,y: x,700) #this 700 is aligned with the number of HBase regions
    data_col4 = data_col3.flatMapValues(lambda xs: xs)
    fdata = data_col4.map ( lambda xs: [xs[0][0],xs[0][1],xs[1][0],xs[1][1]] )

    if ret:
        logging.info ( "OPTIMIZATION RUN!" )
        return fdata.count()
    else:
        logging.info ( "DATABASE RUN!" )
        table_name = hbase_tables[view+1]
        batch_hbase ( host , table_name , fdata )

