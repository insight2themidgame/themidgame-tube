#themidgame tube 

_themidgame-tube_ is a highly scalable data platform built for discoverying YouTube influencers on brand names worldwide.

##Table of Contents
- [Introduction](#markdown-header-introduction)
- [Software Infrastructure](#markdown-header-software-infrastructure)
- [Deployment](#markdown-header-deployment)
- [Data API](#markdown-header-data-api)
- [Pipeline Architecture](#markdown-header-pipeline-architecture)
- [Scalability](#markdown-header-scalability)
- [Demo](#markdown-header-demo)

##Introduction

_themidgame-tube_ is a highly scalable data platform built to store and serve YouTube data with the purpose of discoverying YouTube influencers on brand names. This project was built as a proof of concept for the [themidgame](https://www.themidgame.com/) as they to move forward to scaling out their business solutions and is not necessarily production-ready.  

The focus of this project is to provide a platform that is  

1. easy to deploy
2. low-maintenance
3. scalable
4. flexible

with the ultimate goal of allowing different data models to be built to answer specific questions of the data. Currently, the platform has implemented a simple (yet relevant) data model that is used to test the pipeline's performance.  

Finally, the platform is ready to deploy more sophisticated data models at scale.

##Software Infrastructure

This project was built on the top of AWS Elastic MapReduce (EMR) using AMI version 3.9.0 with the following software:  

1. Hadoop Amazon 2.4.0
2. Spark 1.3.1
3. HBase 0.94.18

Finally, S3 is used as the raw data storage.

##Deployment

Since _themidgame-tube_ is deployed on the top of EMR, the deployment is fairly simple. To deploy _themidgame-tube_, pass `setup/setup.sh` as a [bootstrap action](http://docs.aws.amazon.com/ElasticMapReduce/latest/DeveloperGuide/emr-plan-bootstrap.html) when instantiating the EMR cluster and the platform is ready.  

Finally you can start the platform by ssh-tunneling into the master node and running `/home/hadoop/themidgame-tube/start.sh`.

##Data API

The platform was built around serving an aggregate of data that came from the [YouTube' API](https://developers.google.com/youtube/) for metrics such as the number of subscribers, and from [http://keepsubs.com](http://keepsubs.com) for extracting transcripts from YouTube videos. This scraping functionality was put together for the purposes of testing the pipeline with the expected data types and loads, and is not production ready. Currently, each data point, i.e. each YouTube video ID, is made up of the following schema of API calls (`APICall`) and GET requests (`GET`):  

```
{
  "transcript": APICall ("http://keepsubs.com"),
  "channel": GET("https://www.googleapis.com/youtube/v3/channels?part=statistics&id={ID}&fields=items/statistics/commentCount,items/statistics/subscriberCount,items/statistics/videoCount,items/statistics/viewCount&key={YOUR_API_KEY}"),
  "description": GET("https://www.googleapis.com/youtube/v3/videos?part=snippet&id={VIDEO_ID}&fields=items/snippet/channelId,items/snippet/publishedAt,items/snippet/title,items/snippet/description,items/snippet/channelTitle&key={YOUR_API_KEY}"),
  "stats": GET("https://www.googleapis.com/youtube/v3/videos?part=statistics&id={VIDEO_ID}&fields=items/statistics/commentCount,items/statistics/dislikeCount,items/statistics/likeCount,items/statistics/viewCount&key={YOUR_API_KEY}"),
  "comments": GET("https://www.googleapis.com/youtube/v3/commentThreads?part=snippet%2Creplies&videoId={videoId}&maxResults=100&key={YOUR_API_KEY}"),
  "container": "{BRAND_INFORMATION}"
}
```

Thus, each data point, represented below as `GetData("{VIDEO_ID}")`, is further aggregated into JSON files as

```
"{VIDEO_ID1}" : GetData("{VIDEO_ID1}"),
"{VIDEO_ID2}" : GetData("{VIDEO_ID2}"),
"{VIDEO_ID3}" : GetData("{VIDEO_ID3}"),
             ...
```

for the different brand names. Finally, all of that data is then stored in S3 which may continuously be updated with different brand data or repeated brand data.  

This data collection scheme is not necessarily optimal and serialization systems such as [AVRO](https://avro.apache.org/) should be considered as schema enforcers. This projected was tested with over 19 million YouTube videos mounting to a total load of over 500 GB.

##Pipeline Architecture

![Pipeline](https://bytebucket.org/insight2themidgame/themidgame-tube/raw/5ab83538b6de26356f0e3e89f94117dea85813de/img/pipeline.PNG)

The pipeline is built as a continously-updating-batch process. For the the purposes of collecting data from YouTube, the data content update and/or discovery is not fast enough to justify real-time streaming complexities. By leveraging S3's safe and scalable storage, this pipeline design avoids the maintenance of Kafka brokers and real-streaming software such as Storm.  

Thus, by having the raw data in S3 with a continously-updating batch, this architecture design completely decouples computation resources from storage resources. That is, the cluster can be safely shutdown for any unexpected reason and rebooted.  

Notice that the data is brought into the platform via MapReduce into HDFS which allows batch jobs to take full advantage of HDFS performance, and provides data locality performance gains when importing bulk importing data into HBase via MapReduce.  

By working with Hadoop, Spark, and HBase, this platform provides a flexible environment from which to develop sophisticated data models at scale.

##Scalability

In order to test the system's scalability, three cluster sizes were used to process 500 GB worth of videos (+19 million videos) through the entire pipeline flow, that is, from the raw data in HDFS to bulk loading into HBase.  

The overall set of Spark jobs consist of reading the data data from HDFS, cleaning and transforming the script data into the correct format for bulk loading HBase through MapReduce via the `TableInputFormat`. See the HBase schema used below.

![HBase Schema](https://bytebucket.org/insight2themidgame/themidgame-tube/raw/e1e1b7cf8a652ba80b01425168bd1597246288c9/img/hbase_schema.PNG)

In the HBase schema above, each row, i.e. video ID, contains the unique set of words found in the transcript as columns and word counts as cells. The `RowKey` is salted for performance with the brand name's first two characters and is furhter composed of a numerical value such as the number of subscribers (for sorting) and video ID.  

For this benchmark, an `m3.xlarge` EC2 instance was used as the master node and `m2.4xlarge` EC2 instances were used as slave nodes (spot instances bidded at $0.1/h). Results follow below.

![Benchmark Table](https://bytebucket.org/insight2themidgame/themidgame-tube/raw/e3080c8a72ad0c3f413925183d6fcde359c67795/img/bench_table.PNG)

![Benchmark Plots](https://bytebucket.org/insight2themidgame/themidgame-tube/raw/05257c263bc062633abe298364445e6e4bb229c0/img/bench_plots.PNG)

As can be seen above, both Spark and HBase, as well as the overall throughput of the system scale linearly with the number of slave nodes and the cost rate. Finally, for a fixed cost, of around $2.00, the platform can process 500 GB of data with increasing performance as the cluster size increases (finishing the job quicker).  

Overall, the entire system scalled linearly with the number of slave nodes in the cluster.

##Demo

A proof-of-concept demo of the platform can be found [here](http://themidgame-tube.xyz/home).

