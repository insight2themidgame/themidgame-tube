#!/bin/bash
cd ~
sudo yum -y update
sudo yum install -y dos2unix
sudo yum install -y htop

hadoop fs -get s3://pmoy-test/pmoy-insight.pem ~/pmoy-insight.pem
chmod 600 ~/pmoy-insight.pem

sudo yum install -y git
hadoop fs -get s3://pmoy-test/id_rsa ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
#`ssh-agent /bin/bash`
ssh-add ~/.ssh/id_rsa
ssh -o StrictHostKeyChecking=no git@bitbucket.org
git clone git@bitbucket.org:insight2themidgame/themidgame-tube.git

cd themidgame-tube
git config core.filemode false
git config user.email pmoy@live.com
git config user.name pmoy
git checkout -b beta origin/beta

cp setup/list_slaves.sh ~
cp setup/get_public.sh ~
cp setup/get_dns.sh ~
cp setup/get_private.sh ~
chmod +x ~/list_slaves.sh
chmod +x ~/get_public.sh
chmod +x ~/get_dns.sh
chmod +x ~/get_private.sh

cd /usr/src
sudo wget https://www.python.org/ftp/python/2.7.10/Python-2.7.10.tgz
sudo tar xzf Python-2.7.10.tgz
cd Python-2.7.10
sudo ./configure
sudo make install
sudo ln -sf /usr/bin/python2.7 /usr/bin/python
sudo ln -sf /usr/bin/python2.7 /usr/local/bin/python
sudo ln -sf /usr/bin/pip-2.7 /etc/alternatives/pip
sudo ln -sf /usr/bin/pip-2.7 /usr/bin/pip-python

sudo yum install -y nodejs npm --enablerepo=epel
cd ~/themidgame-tube/scraper
npm install

cd ~
git clone git@bitbucket.org:pmoy/setup.git
cd setup
git config core.filemode false
git config user.email pmoy@live.com
git config user.name pmoy
python setup.py

cd ~
#sudo pip install kafka-python
sudo pip install flask
sudo pip install flask_limiter
#sudo pip install elasticsearch
sudo pip install happybase
sudo pip install "ipython[notebook]"

python -c "import nltk; nltk.download('maxent_treebank_pos_tagger')"

sudo easy_install supervisor

cd ~
cd themidgame-tube
chmod +x client.sh
chmod +x thrift.sh
chmod +x start.sh
